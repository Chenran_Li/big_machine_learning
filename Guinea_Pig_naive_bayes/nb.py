from guineapig import *
import re
import math

def tokenize_doc(words):
    words = re.split('\\s+', words)
    res = []
    for word in words:
        word = re.sub('\\W', '', word)
        #if len(word) > 0 and word.isalpha():
        if len(word) > 2 and word.isalpha():
            res.append(word)
    return res

def generate_info(doc):
    pos1 = doc.find('\t')
    pos2 = doc.find('\t', pos1 + 1)
    doc_label_list = doc[pos1+1:pos2].split(',')
    doc_word_list = tokenize_doc(doc[pos2+1:])
    for i in xrange(len(doc_label_list)):
        yield (('*', ), 1)
    for label in doc_label_list:
        yield ((label, ), 1)
        yield ((label, '*'), len(doc_word_list))
        for word in doc_word_list:
            yield ((label, word), 1)

def generate_label_dict((label, count)):
    if len(label) == 1 or (len(label) == 2 and label[1] == '*'):
        yield (label, count)

def reducer_combine_vocab_size_label_dict(accum, field):
    accum[field[0]] = field[1]
    return accum

def simplify_label_dict((name, count_dict)):
    #name = 'dict'
    #element = ('*', ) or (label, ) or (label, '*')
    res = {}
    res['W=*'] = {}
    res['dis'] = {}
    res['total'] = 0
    for label, count in count_dict.iteritems():
        if label[0] == '*':
            res['total'] = count
        elif len(label) == 1:
            res['dis'][label[0]] = count
        else:
            res['W=*'][label[0]] = count
    for key in res['dis']:
        res['dis'][key] = math.log(res['dis'][key] / float(res['total']))
    return res

def flat_train_words((label, count)):
    if len(label) == 2 and label[1] != '*':
        yield (label[1], label[0], count)
def train_word_count_reducer(accum, (word, Y, count)):
    accum[Y] = count
    return accum

def flat_test_doc(doc):
    pos1 = doc.find('\t')
    pos2 = doc.find('\t', pos1 + 1)
    doc_word_list = tokenize_doc(doc[pos2+1:])
    doc_id = doc[:pos1]
    for word in doc_word_list:
        yield (word, doc_id)

def flat_joined(((word1,counts),(word2,ids))):
    for cur_id in ids:
        yield (cur_id, counts)

ALPHA = 1.0
def predict(((doc_id, word_list), label_dict)):
    res = {}
    #vocab = 10000
    #vocab = label_dict['vocab']
    keys = label_dict['dis'].keys()
    for label in keys:
        res[label] = label_dict['dis'][label]
    for word_labels in word_list:
        for label in keys:
            if label in word_labels:
                occur = word_labels[label]
            else:
                occur = 0
                #occur = 1
            res[label] += math.log((float(occur) + ALPHA)/(label_dict['W=*'][label] + ALPHA * 1e5))
            #res[label] += math.log((float(occur))/(label_dict['W=*'][label]))
    best_label = keys[0]
    best_prob = res[best_label]
    for key in res:
        if res[key] > best_prob:
            best_label = key
            best_prob = res[key]
    return (doc_id, best_label, best_prob)

#always subclass Planner
class NB(Planner):
    # params is a dictionary of params given on the command line.
    # e.g. trainFile = params['trainFile']
    params = GPig.getArgvParams()
    trainFile = params['trainFile']
    testFile = params['testFile']

    event_counts = ReadLines(trainFile) | Flatten(by=generate_info) | Group(by=lambda x:x[0], reducingTo=ReduceTo(int, by=lambda accum,x: accum + x[1]))

    label_dict = event_counts | Flatten(by=generate_label_dict) | Group(by=lambda x:'dict', reducingTo=ReduceTo(dict, by=reducer_combine_vocab_size_label_dict)) | ReplaceEach(by=simplify_label_dict)

    train_word_counts = event_counts | Flatten(by=flat_train_words) | Group(by=lambda x:x[0], reducingTo=ReduceTo(dict, by=train_word_count_reducer))

    requests = ReadLines(testFile) | Flatten(by=flat_test_doc) | Group(by=lambda x:x[0], reducingTo=ReduceTo(list, by=lambda accum,x: accum + [x[1]]))
    # list of counts_list
    joined = Join(Jin(train_word_counts, by=lambda(word,counts):word), Jin(requests, by=lambda(word,ids):word)) | Flatten(by=flat_joined) | Group(by=lambda x:x[0], reducingTo=ReduceTo(list, by=lambda accum,x: accum + [x[1]]))
    output = Augment(joined, sideview=label_dict, loadedBy=lambda v:GPig.onlyRowOf(v)) | ReplaceEach(by=predict)

if __name__ == "__main__":
    NB().main(sys.argv)