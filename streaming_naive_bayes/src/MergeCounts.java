import java.util.*;
import java.io.*;

public class MergeCounts{
	public static void main( String [] args ){
		Scanner scanner = new Scanner( new BufferedInputStream( System.in ) );
		int sum = 0;
		String prevKey = null;
		while( scanner.hasNextLine() ){
			String line = scanner.nextLine();
			int delta = 1;
			String key = line;
			int spacePos = line.indexOf( ' ' );
			if( spacePos >= 0 ){
				delta = Integer.parseInt( line.substring( spacePos + 1, line.length() ) );
				key = line.substring( 0, spacePos );
			}

			if( key.equals( prevKey ) ){
				sum += delta;
			}
			else{
				if( prevKey != null  ){
					System.out.println( prevKey + " " + sum );
				}
				prevKey = key;
				sum = delta;
			}
		}
	}
}
