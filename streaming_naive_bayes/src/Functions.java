import java.util.*;

public class Functions{

	static List<String> tokenizeDoc( String doc ){
		String [] words = doc.split( "\\s+" );
		List<String> tokens = new LinkedList<String>();
		for( int i = 0; i < words.length; i++ ){
			words[i] = words[i].replaceAll( "\\W", "" );
			if( words[i].length() > 0 ){
				tokens.add( words[i] );
			}
		}

		return tokens;
	}

	static List<String> tokenizeDoc( String doc, Set<String> testVocab ){
		String [] words = doc.split( "\\s+" );
		List<String> tokens = new LinkedList<String>();
		for( int i = 0; i < words.length; i++ ){
			words[i] = words[i].replaceAll( "\\W", "" );
			if( words[i].length() > 0 ){
				tokens.add( words[i] );
				testVocab.add( words[i] );
			}
		}

		return tokens;
	}
}