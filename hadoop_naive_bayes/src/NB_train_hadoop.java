import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class NB_train_hadoop {
	public static class Map extends MapReduceBase implements
			Mapper<LongWritable, Text, Text, IntWritable> {
		private static final IntWritable one = new IntWritable(1);

		@Override
		public void map(LongWritable key, Text value,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			String line = value.toString();
			int firstTabPos = line.indexOf("\t");
			if (firstTabPos < 0) {
				return;
			}
			int secondTabPos = line.indexOf("\t", firstTabPos + 1);
			if (secondTabPos < 0) {
				return;
			}
			String tagString = line.substring(firstTabPos + 1, secondTabPos)
					.replaceAll("\\s+", "");
			if (tagString.length() == 0) {
				return;
			}
			String[] tags = tagString.split(",");
			String doc = line.substring(secondTabPos + 1, line.length());
			Vector<String> tokens = tokenizeDoc(doc);

			output.collect(new Text("Y=*"), new IntWritable(tags.length));
			for (int i = 0; i < tags.length; i++) {
				output.collect(new Text("Y=" + tags[i]), one);
				for (String curToken : tokens) {
					output.collect(new Text("Y=" + tags[i] + ",W=" + curToken),
							one);
				}
				output.collect(new Text("Y=" + tags[i] + ",W=*"),
						new IntWritable(tokens.size()));
			}
		}

		private static Vector<String> tokenizeDoc(String cur_doc) {
			String[] words = cur_doc.split("\\s+");
			Vector<String> tokens = new Vector<String>();
			for (int i = 0; i < words.length; i++) {
				words[i] = words[i].replaceAll("\\W", "");
				if (words[i].length() > 0) {
					tokens.add(words[i]);
				}
			}

			return tokens;
		}
	}

	public static class Reduce extends MapReduceBase implements
			Reducer<Text, IntWritable, Text, IntWritable> {
		public void reduce(Text key, Iterator<IntWritable> values,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			int sum = 0;
			while (values.hasNext()) {
				sum += values.next().get();
			}
			output.collect(key, new IntWritable(sum));
		}
	}
}
