import java.util.*;
import java.lang.*;

public class NBModel{
	public static final double ALPHA = 0.4;

	private int trainNum = 0;
	private Map<String,Integer> tagCountMap = new HashMap<String,Integer>();
	private Map<String,Integer> tagCooccurrenceMap = new HashMap<String,Integer>();
	private Map<String,Integer> tagTokenMap = new HashMap<String,Integer>();
	private Map<String,Double> tagDistribution = new HashMap<String,Double>();

	public boolean put( String line, Set<String> testVocab ){
		int spacePos = line.indexOf( ' ' );
		if( spacePos < 0 ){
			return false;
		}

		String key = line.substring( 0, spacePos );
		int value = Integer.parseInt( line.substring( spacePos + 1, line.length() ) );
		
		if( key.equals( "*" ) ){
			setTrainNum( value );
			return true;
		}

		if( key.indexOf( "," ) < 0 ){
			addTagCount( key, value );
			return true;
		}

		String tag = key.substring( 0, key.indexOf( "," ) );

		if( key.indexOf( "*" ) > 0 ){
			addTagCooccurrence( tag, value );
			return true;
		}

		String token = key.substring( key.indexOf( ',' ) + 1, key.length() );
		if( testVocab.contains( token ) ){
			addTagToken( tag, token, value );
			return true;
		}

		return false;
	}

	private void calculateTagDistrubution(){
		Set<String> tagSet = getTagSet();
		int domNum = tagSet.size();
		for( String tag: tagSet ){
			double temp = Math.log( ( (double)getTagCount( tag ) + ALPHA ) / ( getTrainNum() + ALPHA * domNum ) );
			this.tagDistribution.put( tag, temp );
		}
	}

	public double getTagDistrubution( String tag ){
		if( this.tagDistribution.isEmpty() ){
			calculateTagDistrubution();
		}
		return this.tagDistribution.get( tag );
	}

	public Set<String> getTagSet(){
		return tagCountMap.keySet();
	}

	private void setTrainNum( int num ){
		this.trainNum = num;
	}

	public int getTrainNum(){
		return this.trainNum;
	}

	private void addTagCount( String tag, int count ){
		this.tagCountMap.put( tag, count );
	}

	public int getTagCount( String tag ){
		return this.tagCountMap.get( tag );
	}

	private void addTagCooccurrence( String tag, int count ){
		this.tagCooccurrenceMap.put( tag, count );
	}

	public int getTagCooccurrence( String tag ){
		return this.tagCooccurrenceMap.get( tag );
	}

	private void addTagToken( String tag, String token, int count ){
		this.tagTokenMap.put( tag + "," + token, count );
	}

	public int getTagToken( String tag, String token ){
		if( !tagTokenMap.containsKey( tag + "," + token ) ){
			return 0;
		}
		return this.tagTokenMap.get( tag + "," + token );
	}
}
