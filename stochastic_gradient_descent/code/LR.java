import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class LR {
	private static int vocabSize;
	private static double learnRate;
	private static double regularCoeff;
	private static int numIter;
	private static int trainSize;

	public static void main(String[] args) {

		if (args.length != 6) {
			// Console.println("Usage: java -Xmx128m LR 10000 0.5 0.1 20 1000 testData.txt");
			// Console.close();
			return;
		}

		vocabSize = Integer.parseInt(args[0]) / 10;
		learnRate = Double.parseDouble(args[1]);
		regularCoeff = Double.parseDouble(args[2]);
		numIter = Integer.parseInt(args[3]);
		trainSize = Integer.parseInt(args[4]);

		File testFile = new File(args[5]);
		if (!testFile.exists()) {
			// Console.println("test file not exist");
			// Console.close();
			return;
		}

		LR[] lrModels = new LR[14];
		for (int i = 0; i < 14; i++) {
			lrModels[i] = new LR();
		}
		String line = null;
		// train
		Scanner in = new Scanner(System.in);
		for (int pass = 1; pass <= numIter; pass++) {
			double lambda = learnRate / (pass * pass);
			for (int train = 0; train < trainSize; train++) {
				line = in.nextLine();
				int tabPos = line.indexOf("\t");
				String tagString = line.substring(0, tabPos);
				String[] tags = tagString.split(",");
				int[] realTags = toRealTag(tags);
				String doc = line.substring(tabPos + 1, line.length());
				HashSet<Integer> tokens = Functions.tokenizeDoc(doc, vocabSize);
				for (int i = 0; i < 14; i++) {
					lrModels[i].update(tokens, lambda, realTags[i]);
				}
			}
			if (pass == numIter) {
				for (int i = 0; i < 14; i++) {
					lrModels[i].updateB(lambda);
				}
			}
		}

		// test
		try {
			BufferedReader reader = new BufferedReader(new FileReader(testFile));
			line = null;
			while ((line = reader.readLine()) != null) {
				int tabPos = line.indexOf("\t");

				String doc = line.substring(tabPos + 1, line.length());
				HashSet<Integer> tokens = Functions.tokenizeDoc(doc, vocabSize);
				for (int i = 0; i < 14; i++) {
					double prob = lrModels[i].finalP(tokens);
					if (i == 13) {
						System.out.println(TAGS[i] + "\t" + prob);
					} else {
						System.out.print(TAGS[i] + "\t" + prob + ",");
					}
				}
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Console.close();
	}

	private static int[] toRealTag(String[] tags) {
		int[] realTags = new int[14];
		for (int i = 0; i < tags.length; i++) {
			realTags[tagToNum(tags[i])] = 1;
		}
		return realTags;
	}

	private static final String[] TAGS = { "nl", "el", "ru", "sl", "pl", "ca",
			"fr", "tr", "hu", "de", "hr", "es", "ga", "pt" };

	private static int tagToNum(String tag) {
		if (tag.equals("nl"))
			return 0;
		else if (tag.equals("el"))
			return 1;
		else if (tag.equals("ru"))
			return 2;
		else if (tag.equals("sl"))
			return 3;
		else if (tag.equals("pl"))
			return 4;
		else if (tag.equals("ca"))
			return 5;
		else if (tag.equals("fr"))
			return 6;
		else if (tag.equals("tr"))
			return 7;
		else if (tag.equals("hu"))
			return 8;
		else if (tag.equals("de"))
			return 9;
		else if (tag.equals("hr"))
			return 10;
		else if (tag.equals("es"))
			return 11;
		else if (tag.equals("ga"))
			return 12;
		else if (tag.equals("pt"))
			return 13;
		// error
		return -1;
	}

	private static double sigmoid(double score) {
		if (score > overflow) {
			score = overflow;
		} else if (score < -overflow) {
			score = -overflow;
		}
		double exp = Math.exp(score);
		return exp / (1 + exp);
	}

	private int k = 0;
	private Map<Integer, Double> B = new HashMap<Integer, Double>();
	private Map<Integer, Integer> A = new HashMap<Integer, Integer>();
	private static final double overflow = 20.0;

	public double finalP(HashSet<Integer> tokens) {
		double power = 0;
		for (int index : tokens) {
			if (B.containsKey(index)) {
				power += B.get(index);
			}
		}
		return sigmoid(power);
	}

	private double getP(HashSet<Integer> tokens) {
		if (B.isEmpty()) {
			return 0.5;
		}
		double power = 0;
		for (int index : tokens) {
			if (B.containsKey(index)) {
				power += B.get(index);
			}
		}
		return sigmoid(power);
	}

	private void update(HashSet<Integer> tokens, double lambda, int y) {
		double p = getP(tokens);
		for (int index : tokens) {
			if (!B.containsKey(index)) {
				B.put(index, lambda * (y - p));
				A.put(index, k);
			} else {
				double bj = B.get(index)
						* Math.pow(1 - regularCoeff * lambda, k - A.get(index));
				B.put(index, bj + lambda * (y - p));
				A.put(index, k);
			}
		}
		k++;

		return;
	}

	private void updateB(double lambda) {
		Iterator<Entry<Integer, Double>> it = B.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Integer, Double> pair = it.next();
			int key = (Integer) pair.getKey();
			double value = (Double) pair.getValue();
			B.put(key,
					value * Math.pow(1 - regularCoeff * lambda, k - A.get(key)));
		}
	}
}
