import java.util.*;
import java.io.*;
import java.lang.*;

public class NBTest{
	public static void main( String [] args ) throws IOException{
		if( args.length != 1 ){
			System.out.println( "Usage: java -Xmx128m NBTest test.txt" );
			return;
		}

		File testFile = new File( args[0] );
		if( !testFile.exists() ){
			System.out.println( "Error: file " + args[0] + " not exist." );
			return;
		}

		List<List<String>> testDocs = new LinkedList<List<String>>();
		Set<String> testVocab = new HashSet<String>();
		BufferedReader br = new BufferedReader( new FileReader( testFile ) );
		String line = null;
		while( ( line = br.readLine() ) != null ){
			int tabPos = line.indexOf( "\t" );
			if( tabPos < 0 ){
				continue;
			}

			String doc = line.substring( tabPos + 1, line.length() );
			List<String> tokens = Functions.tokenizeDoc( doc, testVocab );
			testDocs.add( tokens );
		}
		br.close();

		NBModel model = new NBModel();
		Scanner scanner = new Scanner( new BufferedInputStream( System.in ) );
		while( scanner.hasNextLine() ){
			line = scanner.nextLine();
			model.put( line, testVocab );
		}

		int testVocabSize = testVocab.size();
		Iterator<List<String>> docItr = testDocs.iterator();
		while( docItr.hasNext() ){
			List<String> curDoc = docItr.next();
			Set<String> tagSet = model.getTagSet();
			double max = 1;
			String bestTag = null;
			for( String tag: tagSet ){
				double logProb = model.getTagDistrubution( tag );
				Iterator<String> itr = curDoc.iterator();
				while( itr.hasNext() ){
					String token = itr.next();
					logProb += Math.log( ( (double)model.getTagToken( tag, token ) + NBModel.ALPHA ) / ( testVocabSize + NBModel.ALPHA * model.getTagCooccurrence( tag ) ) );
				}
				if( bestTag == null ){ // first loop
					max = logProb;
					bestTag = tag;
				}
				else{
					if( logProb > max ){
						max = logProb;
						bestTag = tag;
					}
				}
			}
			System.out.println( bestTag + "\t" + max );
		}
	}
}
