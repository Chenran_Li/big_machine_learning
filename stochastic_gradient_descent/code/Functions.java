import java.util.HashSet;

public class Functions {
	public static HashSet<Integer> tokenizeDoc(String doc, int vocabSize) {
		HashSet<Integer> tokens = new HashSet<Integer>();
		String[] words = doc.split("\\s+");
		for (int i = 0; i < words.length; i++) {
			if (words[i].length() > 2 && isAlpha(words[i])) {
				tokens.add(getId(words[i], vocabSize));
			}
		}
		return tokens;
	}

	private static int getId(String word, int vocabSize) {
		int id = word.hashCode() % vocabSize;
		if (id < 0)
			return id + vocabSize;
		return id;
	}

	public static boolean isAlpha(String name) {
		char[] chars = name.toCharArray();

		for (char c : chars) {
			if (!Character.isLetter(c)) {
				return false;
			}
		}
		return true;
	}
}