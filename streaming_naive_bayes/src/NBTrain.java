import java.util.*;
import java.io.*;

public class NBTrain{
	public static void main( String [] args ){
		Scanner scanner = new Scanner( new BufferedInputStream( System.in ) );
		while( scanner.hasNextLine() ){
			String line = scanner.nextLine();
			int tabPos = line.indexOf( "\t" );
			if( tabPos < 0 ){
				continue;
			}

			String tagString = line.substring( 0, tabPos ).replaceAll( "\\s+", "" );
			if( tagString.length() == 0 ){
				continue;
			}
			String [] tags = tagString.split( "," );

			String doc = line.substring( tabPos + 1, line.length() );
			List<String> tokens = Functions.tokenizeDoc( doc );
			
			inc( "*", tags.length );
			for( int i = 0; i < tags.length; i++ ){
				inc( tags[i], 1 );
				Iterator<String> itr = tokens.iterator();
				while( itr.hasNext() ){
					String curWord = itr.next();
					inc( tags[i] + "," + curWord, 1 );
				}
				inc( tags[i] + ",*", tokens.size() );
			}
		}

		flushTable();
	}

	private static Map<String,Integer> hashTable = new HashMap<String,Integer>();
	private static final int BUFFER_SIZE = 0;

	private static void flushTable(){
		for( Map.Entry<String,Integer> entry : hashTable.entrySet() ){
			print( entry.getKey(), entry.getValue() );
		}
	}

	private static void inc( String word, int delta ){
		if( BUFFER_SIZE == 0 ){
			print( word, delta );
		}
		else{
			if( hashTable.containsKey( word ) ){
				hashTable.put( word, hashTable.get( word ) + delta );
			}
			else{
				hashTable.put( word, delta );
				if( hashTable.size() > BUFFER_SIZE ){
					for( Map.Entry<String,Integer> entry : hashTable.entrySet() ){
						print( entry.getKey(), entry.getValue() );
					}
					hashTable.clear();
				}
			}
		}
	}

	private static void print( String word, int delta ){
		if( delta == 1 ){
			System.out.println( word );
		}
		else{
			System.out.println( word + " " + delta );
		}
	}
}
